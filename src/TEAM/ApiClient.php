<?php

namespace TEAM;

/**
 * Api Client Class
 *
 * Complements ApiController based results
 *
 * @package		team-api-client
 * @author		Mike Frank <mike.frank@teamsystems.ca>
 */
class ApiClient
{
    const USER_AGENT = 'TEAM Framework/API Client';

    protected static $_aJsonMessages = array(
        JSON_ERROR_DEPTH => 'The maximum stack depth has been exceeded',
        JSON_ERROR_STATE_MISMATCH => 'Invalid or malformed JSON',
        JSON_ERROR_CTRL_CHAR => 'JSON control character error, possibly incorrectly encoded',
        JSON_ERROR_SYNTAX => 'JSON syntax error',
        JSON_ERROR_UTF8 => 'Malformed UTF-8 characters in JSON, possibly incorrectly encoded'
    );

    /**
     * Request object
     *
     * @var \TEAM\HttpClient\Request
     */
    public $oRequest;

    /**
     * Last HTTP Response class
     *
     * @var \TEAM\HttpClient\Response
     */
    public $oResponse;

    /**
     * Api version of last response
     *
     * @var string
     */
    public $sApiVersion;

    /**
     * Url of API
     *
     * @var string
     */
    protected $_sUrl;

    /**
     * Options
     *
     * @var array
     */
    protected $_aOptions = array();

    /**
     *
     * @param string $sRootUrl
     * @param array $aOptions
     */
    public function __construct($sRootUrl, array $aOptions = array())
    {
        $this->_sUrl = $sRootUrl;

        $this->_aOptions = array_merge(array(
            'classmap' => array()
        ), $aOptions);

        $this->oRequest = new \TEAM\HttpClient\Request();
    }

    /**
     * Update the root url
     *
     * @param string $sRootUrl
     */
    public function setRoot($sRootUrl)
    {
        $this->_sUrl = $sRootUrl;
    }

    /**
     *  Make a POST request to an API action
     *
     * @param string $sAction
     * @param array $aArgs
     * @return mixed
     */
    public function post($sAction, array $aArgs = array())
    {
        $this->oRequest->setUrl($this->_buildUrl($sAction))
            ->setMethod(\TEAM\HttpClient\Request::METHOD_POST)
            ->addHeader('Accept', 'application/json')
            ->body($aArgs);

        return $this->send();
    }

    /**
     * Make a GET request to an API action
     *
     * @param string $sAction
     * @param array $aArgs
     * @return mixed
     */
    public function get($sAction, array $aArgs = array())
    {
        $this->oRequest->setUrl($this->_buildUrl($sAction))
            ->setMethod(\TEAM\HttpClient\Request::METHOD_GET)
            ->addHeader('Accept', 'application/json')
            ->body($aArgs);

        return $this->send();
    }

    /**
     * Make a GET request to an API action
     *
     * @param string $sAction
     * @param array $aArgs
     * @return mixed
     */
    public function delete($sAction, array $aArgs = array())
    {
        $this->oRequest->setUrl($this->_buildUrl($sAction))
            ->setMethod(\TEAM\HttpClient\Request::METHOD_DELETE)
            ->addHeader('Accept', 'application/json')
            ->body($aArgs);

        return $this->send();
    }

    /**
     * Make a PUT request to an API action
     *
     * @param string $sAction
     * @param array $aArgs
     * @return mixed
     */
    public function put($sAction, array $aArgs = array())
    {
        $this->oRequest->setUrl($this->_buildUrl($sAction))
            ->setMethod(\TEAM\HttpClient\Request::METHOD_PUT)
            ->addHeader('Accept', 'application/json')
            ->body($aArgs);

        return $this->send();
    }

    protected function _buildUrl($sAction)
    {
        return rtrim($this->_sUrl, '/') . '/' . trim($sAction, '/') . '/';
    }

    protected function _objectToObject($oInstance, $sClassName)
    {
        // Use Reflection class when PHP 5.4
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($sClassName),
            $sClassName,
            strstr(strstr(serialize($oInstance), '"'), ':')
        ));
    }

// NOT USED AS OF AUGUST-13-2012 - MIKE
//	protected function _arrayToObject(array $aArray, $sClassName)
//	{
//		return unserialize(sprintf(
//			'O:%d:"%s"%s',
//			strlen($sClassName),
//			$sClassName,
//			strstr(serialize($aArray), ':')
//		));
//	}

    protected function _iterateResult(&$mResult) {
        $bToArray = FALSE;

        if ( is_object($mResult) ) {

            $aVars = get_object_vars($mResult);
            foreach($aVars as $sName => $mValue) {

                if ( strpos($sName, '_tf_') === 0 )
                {
                    unset($mResult->$sName);

                    if ( $sName == '_tf_datatype' ) {
                        if ( $mValue == 'array' ) {
                            $bToArray = TRUE;
                        }
                        elseif ( array_key_exists($mValue, $this->_aOptions['classmap']) ) {
                            $mResult = $this->_objectToObject($mResult, $this->_aOptions['classmap'][$mValue]);
                        }
                    }
                }
                else {

                    $this->_iterateResult($mResult->$sName);
                }

            }

        }
        elseif (is_array($mResult)) {

            foreach($mResult as $sName => $mValue) {

                if ( strpos($sName, '_tf_') === 0 )
                {
                    unset($mResult[$sName]);
                }
                else {
                    $this->_iterateResult($mResult[$sName]);
                }
            }

        }

        if ( $bToArray === TRUE ) {
            // Objects must be converted to array after they've been iterated
            $mResult = (array) $mResult;
        }
    }

    /**
     * Send a request
     *
     * @return array
     * @throws \TEAM\Exceptions\ApiClient
     */
    public function send()
    {
        $mReturn = NULL;

        $this->oRequest->setUserAgent(self::USER_AGENT);

        $this->oResponse = $this->oRequest->send();

        if ( $this->oResponse->sContentType == 'application/json' ) {
            $mReturn = json_decode($this->oResponse->sBody);

            if ( ($iError = json_last_error()) ) {
                throw new \TEAM\Exceptions\ApiClient(static::$_aJsonMessages[$iError] . ' on ' . $this->oResponse->sUrl, $this->oResponse);
            }

            // Run though result and parse out special commands
            $this->_iterateResult($mReturn);
        }
        else {
            throw new \TEAM\Exceptions\ApiClient('Received unsupported content type: ' . $this->oResponse->sContentType . ' from ' . $this->oResponse->sUrl, $this->oResponse);
        }

        if ( $this->oResponse->iStatus == 200 ) {

            if ( $this->oResponse->sBody ) {

                if ( isset($mReturn->data) and isset($mReturn->apiVersion) ) {
                    $this->sApiVersion = $mReturn->apiVersion;

                    return $mReturn->data;
                }
                else {
                    // Missing "data" attribute on result, falling back to root element

                    return $mReturn;
                }

            }
            else {
                throw new \TEAM\Exceptions\ApiClient('Empty response body received from ' . $this->oResponse->sUrl, $this->oResponse);
            }
        }
        else {
            if ( isset($mReturn->error->message) ) {
                throw new \TEAM\Exceptions\ApiClient($mReturn->error->message, $this->oResponse);
            }
            else {
                throw new \TEAM\Exceptions\ApiClient('Received invalid HTTP status code: ' . $this->oResponse->iStatus . ' on ' . $this->oResponse->sUrl, $this->oResponse);
            }
        }
    }
}
