<?php

namespace TEAM\Exceptions;

/**
 * Api Client Exception Class
 *
 * @package		team-api-client
 * @author		Mike Frank <mike.frank@teamsystems.ca>
 */
class ApiClient extends \Exception
{
    /**
     *
     * @var \TEAM\HttpClient\Response
     */
    public $oResponse;

    public function __construct($sMessage, \TEAM\HttpClient\Response $oResponse)
    {
        parent::__construct($sMessage);

        $this->oResponse = $oResponse;
    }
}