# TEAM API Client
Used to communicate with output from controllers in TEAM Framework which
use the ApiController.

## Install with Composer
To install with [Composer](https://getcomposer.org/), simply require the latest version of this package.
```
composer require team/apiclient
```